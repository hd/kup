FROM matejak/argbash
WORKDIR /
COPY kup.m4 .
RUN \
    argbash kup.m4 -o kup.sh

FROM debian:10-slim
COPY scripts scripts
COPY --from=0 kup.sh .
