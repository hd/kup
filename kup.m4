#!/bin/bash
#
# ARG_OPTIONAL_BOOLEAN([verbose], , [print verbose output])
# ARG_POSITIONAL_SINGLE([hostname], [host name to set], )
# ARG_POSITIONAL_SINGLE([path], [mount path to patch ], )
# ARG_HELP([kup - helps to easily setup a kubernetes worker node])
# ARGBASH_SET_INDENT([  ])
# DEFINE_SCRIPT_DIR([_script_dir])
# ARGBASH_GO

# [ <-- needed because of Argbash

# ANSI escape codes
_RED=$(tput setaf 1)
_GREEN=$(tput setaf 2)
_YELLOW=$(tput setaf 3)
_BLUE=$(tput setaf 4)
_CYAN=$(tput setaf 6)
_WHITE=$(tput setaf 7)
_NC=$(tput sgr0)
_BOLD=$(tput sgr 0 0 0 0 0 1)
_DIM=$(tput sgr 0 0 0 0 1)

echo " ${_GREEN}verbose${_NC} output is ${_BOLD}$_arg_verbose${_NC}"

if [[ $_arg_verbose == 'on' ]]
then
  echo " ${_GREEN}script dir${_NC}  : ${_BOLD}$_script_dir${_NC}"
  echo " ${_GREEN}hostname${_NC}    : ${_BOLD}$_arg_hostname${_NC}"
  echo " ${_GREEN}path${_NC}        : ${_BOLD}$_arg_path${_NC}"
fi

for f in ${_script_dir}/scripts/*.sh; do
  echo "${_DIM} .. $(basename ${f}) ...${_NC}"
  . $f || break
done

# ] <-- needed because of Argbash
