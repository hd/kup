# kup - Kubernetes on RasPi

setup a Kubernetes worker node

## Performs the following tasks

- [x] updates hostname
- [x] removes the `popularity-contest` cron job
- [ ] replaces default password
- [ ] places the given public SSH key for accessing over `ssh`
- [ ] [enable c-groups](https://ubuntu.com/tutorials/how-to-kubernetes-cluster-on-raspberry-pi#4-installing-microk8s)
- [ ] create a daily update cron job
- [ ] create first login script
  - [ ] call update on first login
  - [ ] install micro8s: `sudo snap install microk8s --classic`
  - [ ] install master node `sudo microk8s.add-node` -> generates token
  - [ ] join master node `microk8s.join 10.55.60.14:25000/<token>`
  - [ ] add the user ubuntu to the 'microk8s' group:

    ```bash
    sudo usermod -a -G microk8s ubuntu
    sudo chown -f -R ubuntu ~/.kube
    ```
  
  - [ ] test
